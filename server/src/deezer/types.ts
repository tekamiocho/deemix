export type PaginationOptions = { index?: number; limit?: number }

export type DeezerGenre = {
  id: number
  name: string
  picture: string
  type: 'genre'

  picture_small?: string | null
  picture_medium?: string | null
  picture_big?: string | null
  picture_xl?: string | null

  [key: string]: any
}

export type DeezerArtist = {
  id: string
  name: string
  tracklist: string
  type: 'artist'

  picture?: string
  picture_small?: string | null
  picture_medium?: string | null
  picture_big?: string | null
  picture_xl?: string | null
  link?: string
  share?: string
  nb_album?: number
  nb_fan?: number
  radio?: boolean

  [key: string]: any
}

export type DeezerContributor = DeezerArtist & {
  role?: string
}

export type DeezerAlbumWithoutTracks = {
  id: string
  title: string
  cover: string | null
  cover_small: string | null
  cover_medium: string | null
  cover_big: string | null
  cover_xl: string | null
  md5_image: string | null
  tracklist: string
  type: 'album'

  link?: string
  release_date?: string
  upc?: string
  share?: string
  genre_id?: string
  genres?: { data: DeezerGenre[] }
  label?: string
  nb_tracks?: number
  duration?: string
  fans?: number
  record_type?: string
  available?: boolean
  explicit_lyrics?: boolean
  explicit_content_lyrics?: number
  explicit_content_cover?: number
  contributors?: DeezerContributor[]
  artist?: DeezerArtist

  [key: string]: any
}

export type DeezerTrack = {
  id: string
  readable: boolean
  title: string
  title_short: string
  title_version: string
  link: string
  duration: string
  explicit_lyrics: boolean
  explicit_content_lyrics: number
  explicit_content_cover: number
  md5_image: string
  artist: DeezerArtist
  type: 'track'

  rank?: string
  preview?: string
  isrc?: string
  share?: string
  track_position?: number
  disk_number?: number
  release_date?: string
  bpm?: number
  gain?: number
  available_countries?: string[]
  contributors?: DeezerContributor[]
  album?: DeezerAlbumWithoutTracks

  [key: string]: any
}

export type DeezerAlbumWithTracks = DeezerAlbumWithoutTracks & {
  tracks: DeezerTrack[]
}

export type DeezerAlbum = DeezerAlbumWithoutTracks | DeezerAlbumWithTracks

export type DeezerPlaylistTrack = DeezerTrack & {
  time_add: number
}

export type DeezerUser = {
  id: string
  name: string
  tracklist: string
  type: 'user'

  link?: string
  picture?: string
  picture_small?: string
  picture_medium?: string
  picture_big?: string
  picture_xl?: string
  country?: string

  [key: string]: any
}

export type DeezerPlaylist = {
  id: string
  title: string
  description: string
  duration: number
  public: boolean
  is_loved_track: boolean
  collaborative: boolean
  nb_tracks: number
  fans: number
  link: string
  share: string
  picture: string
  picture_small: string
  picture_medium: string
  picture_big: string
  picture_xl: string
  checksum: string
  tracklist: string
  creation_date: string
  md5_image: string
  picture_type: string
  creator: DeezerUser
  type: 'playlist'

  tracks?: DeezerPlaylistTrack[]

  [key: string]: any
}
