class DeezerError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'DeezerError'
  }
}

class WrongLicense extends DeezerError {
  format: string

  constructor(format: string) {
    super(`Your account can't request urls for ${format} tracks`)
    this.name = 'WrongLicense'
    this.format = format
  }
}

class WrongGeolocation extends DeezerError {
  country: string

  constructor(country: string) {
    super(`The track you requested can't be streamed in country ${country}`)
    this.name = 'WrongGeolocation'
    this.country = country
  }
}

// APIError
class APIError extends DeezerError {
  constructor(message: string) {
    super(message)
    this.name = 'APIError'
  }
}
class ItemsLimitExceededException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'ItemsLimitExceededException'
  }
}
class PermissionException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'PermissionException'
  }
}
class InvalidTokenException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'InvalidTokenException'
  }
}
class WrongParameterException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'WrongParameterException'
  }
}
class MissingParameterException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'MissingParameterException'
  }
}
class InvalidQueryException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'InvalidQueryException'
  }
}
class DataException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'DataException'
  }
}
class IndividualAccountChangedNotAllowedException extends APIError {
  constructor(message: string) {
    super(message)
    this.name = 'IndividualAccountChangedNotAllowedException'
  }
}
class GWAPIError extends DeezerError {
  constructor(message: string) {
    super(message)
    this.name = 'GWAPIError'
  }
}

export {
  DeezerError,
  WrongLicense,
  WrongGeolocation,
  APIError,
  ItemsLimitExceededException,
  PermissionException,
  InvalidTokenException,
  WrongParameterException,
  MissingParameterException,
  InvalidQueryException,
  DataException,
  IndividualAccountChangedNotAllowedException,
  GWAPIError
}
