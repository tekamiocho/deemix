import { Listener } from '../../types'

type DownloadItemData = {
  id?: string
  title?: string
  artist?: string
  errorPosition?: string
}

type DownloadObjectError = {
  message: string
  errid?: string
  data: DownloadItemData
  stack?: string
  type: string
}

type DownloadResult = {
  error?: DownloadObjectError
  filename?: string
  data?: DownloadItemData
  path?: string
  searched?: boolean
  albumURLs?: { url: string; ext: string }[]
  albumPath?: string
  albumFilename?: string
  artistURLs?: { url: string; ext: string }[]
  artistPath?: string
  artistFilename?: string
}

class IDownloadObject {
  type: string
  id: string
  bitrate: number
  title: string
  artist: string
  cover: string
  explicit: boolean
  size: number
  downloaded: number
  failed: number
  progress: number
  errors: DownloadObjectError[]
  files: DownloadResult[]
  extrasPath: string
  progressNext: number
  uuid: string
  isCanceled: boolean
  __type__: string

  constructor(obj: any) {
    this.type = obj.type
    this.id = obj.id
    this.bitrate = obj.bitrate
    this.title = obj.title
    this.artist = obj.artist
    this.cover = obj.cover
    this.explicit = obj.explicit || false
    this.size = obj.size
    this.downloaded = obj.downloaded || 0
    this.failed = obj.failed || 0
    this.progress = obj.progress || 0
    this.errors = obj.errors || []
    this.files = obj.files || []
    this.extrasPath = obj.extrasPath || ''
    this.progressNext = 0
    this.uuid = `${this.type}_${this.id}_${this.bitrate}`
    this.isCanceled = false
    this.__type__ = ''
  }

  toDict(): any {
    return {
      type: this.type,
      id: this.id,
      bitrate: this.bitrate,
      uuid: this.uuid,
      title: this.title,
      artist: this.artist,
      cover: this.cover,
      explicit: this.explicit,
      size: this.size,
      downloaded: this.downloaded,
      failed: this.failed,
      progress: this.progress,
      errors: this.errors,
      files: this.files,
      extrasPath: this.extrasPath,
      __type__: this.__type__
    }
  }

  getResettedDict(): any {
    const item = this.toDict()
    item.downloaded = 0
    item.failed = 0
    item.progress = 0
    item.errors = []
    item.files = []
    return item
  }

  getSlimmedDict(): any {
    const light: any = this.toDict()
    const propertiesToDelete = ['single', 'collection', 'plugin', 'conversion_data']
    propertiesToDelete.forEach(property => {
      if (Object.keys(light).includes(property)) {
        delete light[property]
      }
    })
    return light
  }

  getEssentialDict(): any {
    return {
      type: this.type,
      id: this.id,
      bitrate: this.bitrate,
      uuid: this.uuid,
      title: this.title,
      artist: this.artist,
      cover: this.cover,
      explicit: this.explicit,
      size: this.size,
      extrasPath: this.extrasPath
    }
  }

  updateProgress(listener: Listener | undefined) {
    if (Math.floor(this.progressNext) !== this.progress && Math.floor(this.progressNext) % 2 === 0) {
      this.progress = Math.floor(this.progressNext)
      if (listener) listener.send('updateQueue', { uuid: this.uuid, progress: this.progress })
    }
  }
}

class Single extends IDownloadObject {
  single: any

  constructor(obj: any) {
    super(obj)
    this.size = 1
    this.single = obj.single
    this.__type__ = 'Single'
  }

  toDict() {
    const item = super.toDict()
    item.single = this.single
    return item
  }

  completeTrackProgress(listener: Listener | undefined) {
    this.progressNext = 100
    this.updateProgress(listener)
  }

  removeTrackProgress(listener: Listener | undefined) {
    this.progressNext = 0
    this.updateProgress(listener)
  }
}

class Collection extends IDownloadObject {
  collection: any

  constructor(obj: any) {
    super(obj)
    this.collection = obj.collection
    this.__type__ = 'Collection'
  }

  toDict(): any {
    const item = super.toDict()
    item.collection = this.collection
    return item
  }

  completeTrackProgress(listener: Listener | undefined) {
    this.progressNext += (1 / this.size) * 100
    this.updateProgress(listener)
  }

  removeTrackProgress(listener: Listener | undefined) {
    this.progressNext -= (1 / this.size) * 100
    this.updateProgress(listener)
  }
}

class Convertable extends Collection {
  plugin: string
  conversion_data: any

  constructor(obj: any) {
    super(obj)
    this.plugin = obj.plugin
    this.conversion_data = obj.conversion_data
    this.__type__ = 'Convertable'
  }

  toDict() {
    const item = super.toDict()
    item.plugin = this.plugin
    item.conversion_data = this.conversion_data
    return item
  }
}

export { IDownloadObject, Single, Collection, Convertable, DownloadObjectError, DownloadResult, DownloadItemData }
