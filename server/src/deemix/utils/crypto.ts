import crypto from 'crypto'
import { Blowfish } from 'egoroof-blowfish'

type BufferEncoding = 'ascii' | 'utf8' | 'utf-8' | 'utf16le' | 'ucs2' | 'ucs-2' | 'base64' | 'latin1' | 'binary' | 'hex'

function _md5(data: string, type: BufferEncoding = 'binary') {
  const md5sum = crypto.createHash('md5')
  md5sum.update(Buffer.from(data, type))
  return md5sum.digest('hex')
}

function _ecbCrypt(key: string, data: string) {
  const cipher = crypto.createCipheriv('aes-128-ecb', Buffer.from(key), Buffer.from(''))
  cipher.setAutoPadding(false)
  return Buffer.concat([cipher.update(data, 'binary'), cipher.final()])
    .toString('hex')
    .toLowerCase()
}

function _ecbDecrypt(key: string, data: string) {
  const cipher = crypto.createDecipheriv('aes-128-ecb', Buffer.from(key), Buffer.from(''))
  cipher.setAutoPadding(false)
  return Buffer.concat([cipher.update(data, 'binary'), cipher.final()])
    .toString('hex')
    .toLowerCase()
}

function generateBlowfishKey(trackId: string) {
  const SECRET = 'g4el58wc0zvf9na1'
  const idMd5 = _md5(trackId, 'ascii')
  let bfKey = ''
  for (let i = 0; i < 16; i++) {
    bfKey += String.fromCharCode(idMd5.charCodeAt(i) ^ idMd5.charCodeAt(i + 16) ^ SECRET.charCodeAt(i))
  }
  return String(bfKey)
}

function decryptChunk(chunk: Buffer, blowFishKey: string) {
  const ciphers = crypto.getCiphers()
  if (ciphers.includes('bf-cbc')) {
    const cipher = crypto.createDecipheriv('bf-cbc', blowFishKey, Buffer.from([0, 1, 2, 3, 4, 5, 6, 7]))
    cipher.setAutoPadding(false)
    return Buffer.concat([cipher.update(chunk), cipher.final()])
  }
  if (Blowfish) {
    const cipher = new Blowfish(blowFishKey, Blowfish.MODE.CBC, Blowfish.PADDING.NULL)
    cipher.setIv(Buffer.from([0, 1, 2, 3, 4, 5, 6, 7]))
    return Buffer.from(cipher.decode(chunk, Blowfish.TYPE.UINT8_ARRAY))
  }
  throw new Error("Can't find a way to decrypt chunks")
}

export { _md5, _ecbCrypt, _ecbDecrypt, generateBlowfishKey, decryptChunk }
