import got from 'got'
import { CookieJar } from 'tough-cookie'
import { _md5 } from './crypto'
import { USER_AGENT_HEADER } from './index'

const CLIENT_ID = '447462'
const CLIENT_SECRET = 'a83bf7f38ad2f137e444727cfc3775cf'

async function loginViaEmail(email: string, password: string) {
  const cookieJar = new CookieJar()
  let accessToken: null | string = null
  let arl: null | string = null

  // Server sends set-cookie header with new sid
  await got.post('https://www.deezer.com', {
    headers: { 'User-Agent': USER_AGENT_HEADER },
    https: { rejectUnauthorized: false },
    cookieJar
  })

  // Hash password
  password = _md5(password, 'utf8')
  const hash = _md5([CLIENT_ID, email, password, CLIENT_SECRET].join(''), 'utf8')

  // Server sends set-cookie header with account sid
  try {
    const response: any = await got
      .get('https://connect.deezer.com/oauth/user_auth.php', {
        searchParams: {
          app_id: CLIENT_ID,
          login: email,
          password,
          hash
        },
        https: { rejectUnauthorized: false },
        headers: { 'User-Agent': USER_AGENT_HEADER },
        cookieJar
      })
      .json()
    accessToken = response.access_token
    if (accessToken === 'undefined') accessToken = null
  } catch {
    /* empty */
  }

  // Get ARL
  try {
    const response: any = await got
      .get('https://www.deezer.com/ajax/gw-light.php?method=user.getArl&input=3&api_version=1.0&api_token=null', {
        headers: { 'User-Agent': USER_AGENT_HEADER },
        https: { rejectUnauthorized: false },
        cookieJar
      })
      .json()
    arl = response.results
  } catch {
    /* empty */
  }
  return arl
}

async function getAccessToken(email: string, password: string) {
  let accessToken = null
  password = _md5(password, 'utf8')
  const hash = _md5([CLIENT_ID, email, password, CLIENT_SECRET].join(''), 'utf8')
  try {
    const response: any = await got
      .get('https://api.deezer.com/auth/token', {
        searchParams: {
          app_id: CLIENT_ID,
          login: email,
          password,
          hash
        },
        https: { rejectUnauthorized: false },
        headers: { 'User-Agent': USER_AGENT_HEADER }
      })
      .json()
    accessToken = response.access_token
    if (accessToken === 'undefined') accessToken = null
  } catch {
    /* empty */
  }
  return accessToken
}

async function getArlFromAccessToken(accessToken: string) {
  if (!accessToken) return null
  let arl = null
  const cookieJar = new CookieJar()
  try {
    await got.get('https://api.deezer.com/platform/generic/track/3135556', {
      headers: { Authorization: `Bearer ${accessToken}`, 'User-Agent': USER_AGENT_HEADER },
      https: { rejectUnauthorized: false },
      cookieJar
    })
    const response: any = await got
      .get('https://www.deezer.com/ajax/gw-light.php?method=user.getArl&input=3&api_version=1.0&api_token=null', {
        headers: { 'User-Agent': USER_AGENT_HEADER },
        https: { rejectUnauthorized: false },
        cookieJar
      })
      .json()
    arl = response.results
  } catch {
    /* empty */
  }
  return arl
}

export { getAccessToken, getArlFromAccessToken, loginViaEmail }
