import fs from 'fs'
import { Transform, TransformOptions } from 'stream'
import got from 'got'
import { Listener } from '../types'
import { _md5, _ecbCrypt, _ecbDecrypt, generateBlowfishKey, decryptChunk } from './utils/crypto'
import { DownloadCanceled, DownloadEmpty } from './errors'

import { USER_AGENT_HEADER, pipeline } from './utils/index'
import { Track } from './types/Track'
import { IDownloadObject } from './types/DownloadObjects'

class Decrypter extends Transform {
  private modifiedStream: Buffer
  private isCryptedStream: boolean
  private blowfishKey: string | undefined

  constructor(options: TransformOptions, blowfishKey: string | undefined) {
    super(options)
    this.modifiedStream = Buffer.alloc(0)
    this.isCryptedStream = blowfishKey !== undefined
    this.blowfishKey = blowfishKey
  }

  _transform(chunk: Buffer, _encoding: string, callback: (error?: Error | null, data?: any) => void) {
    if (!this.isCryptedStream) {
      this.push(chunk)
    } else {
      this.modifiedStream = Buffer.concat([this.modifiedStream, chunk])
      while (this.modifiedStream.length >= 2048 * 3) {
        let decryptedChunks = Buffer.alloc(0)
        const decryptingChunks = this.modifiedStream.slice(0, 2048 * 3)
        this.modifiedStream = this.modifiedStream.slice(2048 * 3)
        if (decryptingChunks.length >= 2048) {
          decryptedChunks = decryptChunk(decryptingChunks.slice(0, 2048), this.blowfishKey!)
          decryptedChunks = Buffer.concat([decryptedChunks, decryptingChunks.slice(2048)])
        }
        this.push(decryptedChunks)
      }
    }
    callback()
  }

  _flush(callback: (error?: Error | null, data?: any) => void) {
    if (this.isCryptedStream) {
      let decryptedChunks = Buffer.alloc(0)
      if (this.modifiedStream.length >= 2048) {
        decryptedChunks = decryptChunk(this.modifiedStream.slice(0, 2048), this.blowfishKey!)
        decryptedChunks = Buffer.concat([decryptedChunks, this.modifiedStream.slice(2048)])
        this.push(decryptedChunks)
      } else {
        this.push(this.modifiedStream)
      }
    }
    callback()
  }
}

class Depadder extends Transform {
  private isStart: boolean

  constructor(options: TransformOptions) {
    super(options)
    this.isStart = true
  }

  _transform(chunk: Buffer, _encoding: string, callback: (error?: Error | null, data?: any) => void) {
    if (this.isStart && chunk[0] === 0 && chunk.slice(4, 8).toString() !== 'ftyp') {
      let i
      for (i = 0; i < chunk.length; i++) {
        const byte = chunk[i]
        if (byte !== 0) break
      }
      chunk = chunk.slice(i)
    }
    this.isStart = false
    this.push(chunk)
    callback()
  }
}

function generateStreamPath(sngID: string, md5: string, mediaVersion: string, format: number) {
  let urlPart = md5 + '¤' + format + '¤' + sngID + '¤' + mediaVersion
  const md5val = _md5(urlPart)
  let step2 = md5val + '¤' + urlPart + '¤'
  step2 += '.'.repeat(16 - (step2.length % 16))
  urlPart = _ecbCrypt('jo6aey6haid2Teih', step2)
  return urlPart
}

function reverseStreamPath(urlPart: string) {
  const step2 = _ecbDecrypt('jo6aey6haid2Teih', urlPart)
  const [, md5, format, sngID, mediaVersion] = step2.split('¤')
  return [sngID, md5, mediaVersion, format]
}

function generateCryptedStreamURL(sngID: string, md5: string, mediaVersion: string, format: number) {
  const urlPart = generateStreamPath(sngID, md5, mediaVersion, format)
  return 'https://e-cdns-proxy-' + md5[0] + '.dzcdn.net/mobile/1/' + urlPart
}

function generateStreamURL(sngID: string, md5: string, mediaVersion: string, format: number) {
  const urlPart = generateStreamPath(sngID, md5, mediaVersion, format)
  return 'https://cdns-proxy-' + md5[0] + '.dzcdn.net/api/1/' + urlPart
}

function reverseStreamURL(url: string) {
  const urlPart = url.slice(url.indexOf('/1/') + 3)
  return reverseStreamPath(urlPart)
}

async function streamTrack(
  writepath: string,
  track: Track,
  downloadObject: IDownloadObject,
  listener: Listener | undefined = undefined
): Promise<void> {
  if (downloadObject && downloadObject.isCanceled) throw new DownloadCanceled()
  const headers = { 'User-Agent': USER_AGENT_HEADER }
  let chunkLength = 0
  let complete = 0
  const isCryptedStream = track.downloadURL!.includes('/mobile/') || track.downloadURL!.includes('/media/')
  let blowfishKey: string | undefined
  const outputStream = fs.createWriteStream(writepath)
  let timeout: ReturnType<typeof setTimeout> | undefined

  const itemData = {
    id: track.id,
    title: track.title,
    artist: track.mainArtist.name
  }
  let error = ''

  if (isCryptedStream) blowfishKey = generateBlowfishKey(track.id)

  const decrypter = new Decrypter({}, blowfishKey)
  const depadder = new Depadder({})

  const request = got
    .stream(track.downloadURL!, {
      headers,
      https: { rejectUnauthorized: false }
    })
    .on('response', response => {
      if (timeout !== undefined) clearTimeout(timeout)
      complete = parseInt(response.headers['content-length'])
      if (complete === 0) {
        error = 'DownloadEmpty'
        request.destroy()
      }
      if (listener)
        listener.send('downloadInfo', {
          uuid: downloadObject.uuid,
          data: itemData,
          state: 'downloading',
          alreadyStarted: false,
          value: complete
        })
    })
    .on('data', function (chunk) {
      if (downloadObject.isCanceled) {
        error = 'DownloadCanceled'
        request.destroy()
      }
      chunkLength += chunk.length

      if (downloadObject) {
        downloadObject.progressNext += (chunk.length / complete / downloadObject.size) * 100
        downloadObject.updateProgress(listener)
      }
      if (timeout !== undefined) clearTimeout(timeout)
      timeout = setTimeout(() => {
        error = 'DownloadTimeout'
        request.destroy()
      }, 5000)
    })

  timeout = setTimeout(() => {
    error = 'DownloadTimeout'
    request.destroy()
  }, 5000)

  try {
    await pipeline(request, decrypter, depadder, outputStream)
  } catch (e: any) {
    if (fs.existsSync(writepath)) fs.unlinkSync(writepath)
    if (
      e instanceof got.ReadError ||
      e instanceof got.TimeoutError ||
      ['ESOCKETTIMEDOUT', 'ERR_STREAM_PREMATURE_CLOSE', 'ETIMEDOUT', 'ECONNRESET'].includes(e.code) ||
      (request.destroyed && error === 'DownloadTimeout')
    ) {
      if (downloadObject && chunkLength !== 0) {
        downloadObject.progressNext -= (chunkLength / complete / downloadObject.size) * 100
        downloadObject.updateProgress(listener)
      }
      if (listener)
        listener.send('downloadInfo', {
          uuid: downloadObject.uuid,
          data: itemData,
          state: 'downloadTimeout'
        })
      return await streamTrack(writepath, track, downloadObject, listener)
    } else if (request.destroyed) {
      switch (error) {
        case 'DownloadEmpty':
          throw new DownloadEmpty()
        case 'DownloadCanceled':
          throw new DownloadCanceled()
        default:
          throw e
      }
    } else {
      console.trace(e)
      throw e
    }
  }
}

export {
  generateStreamPath,
  generateStreamURL,
  generateCryptedStreamURL,
  reverseStreamPath,
  reverseStreamURL,
  streamTrack
}
