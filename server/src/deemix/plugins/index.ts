import { Deezer } from '../../deezer'
import { Listener } from '../../types'
import { Collection, Convertable, Single } from '../types/DownloadObjects'

export interface Plugin {
  setup(): void

  parseLink(link: string): Promise<(string | undefined)[]>

  generateDownloadObject(
    dz: Deezer,
    link_url: string,
    bitrate: number,
    listener: Listener | undefined
  ): Promise<Single | Collection | Convertable | undefined>
}
