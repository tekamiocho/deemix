import { ApiHandler } from '../../../types'
import { loginViaEmail } from '../../../deemix/utils/deezer'
import { saveLoginCredentials } from '../../../helpers/loginStorage'

const path = '/loginEmail'

const handler: ApiHandler['handler'] = async (req, res) => {
  const isSingleUser = req.app.get('isSingleUser')
  const { email, password } = req.body

  const arl = await loginViaEmail(email, password)
  console.log(arl)

  if (isSingleUser && arl)
    saveLoginCredentials({
      accessToken: null,
      arl: arl || null
    })

  res.send({ accessToken: null, arl })
}

const apiHandler = { path, handler }

export default apiHandler
