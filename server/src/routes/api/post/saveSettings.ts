import { ApiHandler } from '../../../types'
import { Settings } from '../../../deemix/types/Settings'

const path = '/saveSettings'

export interface SaveSettingsData {
  settings: Settings
  spotifySettings: any
}

const handler: ApiHandler['handler'] = (req, res) => {
  const deemix = req.app.get('deemix')
  const { settings, spotifySettings }: SaveSettingsData = req.query
  deemix.saveSettings(settings, spotifySettings)
  deemix.listener.send('updateSettings', { settings, spotifySettings })
  res.send({ result: true })
}

const apiHandler = { path, handler }

export default apiHandler
