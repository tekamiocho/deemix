declare module 'metaflac-js2' {
  class Metaflac {
    constructor(flac: string | Buffer)
    setTag(field: string): void
    setTagFromFile(field: string): void
    importTagsFrom(filename: string): void
    exportTagsTo(filename: string): void
    importPicture(picture: string | Buffer): void
    exportPictureTo(filename: string): void
    getAllTags(): string[]
    removeAllTags(): void

    buildSpecification(spec?: {
      type?: number
      mime?: string
      description?: string
      width?: number
      height?: number
      depth?: number
      colors?: number
    }): {
      type: number
      mime: string
      description: string
      width: number
      height: number
      depth: number
      colors: number
    }

    buildPictureBlock(
      picture: Buffer,
      specification?: {
        type?: number
        mime?: string
        description?: string
        width?: number
        height?: number
        depth?: number
        colors?: number
      }
    ): Buffer

    buildMetadataBlock(type: number, block: Buffer, isLast?: boolean): Buffer
    buildMetadata(): Buffer[]
    buildStream(): Buffer[]
    save(): void | Buffer
  }

  export = Metaflac
}
