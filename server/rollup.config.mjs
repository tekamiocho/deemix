import typescript from '@rollup/plugin-typescript'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import json from '@rollup/plugin-json'
import replace from 'rollup-plugin-re'
import terser from '@rollup/plugin-terser'

export default {
  input: 'src/index.ts',
  output: {
    dir: 'dist',
    format: 'cjs',
    sourcemap: false,
    chunkFileNames: '[name].js'
  },
  plugins: [
    typescript(),
    nodeResolve({
      preferBuiltins: true
    }),
    // Fix for formidable require reassignment
    replace({
      patterns: [
        {
          match: /formidable(\/|\\)lib/,
          test: 'if (global.GENTLY) require = GENTLY.hijack(require);',
          replace: ''
        }
      ]
    }),
    commonjs(),
    json(),
    terser()
  ]
}
